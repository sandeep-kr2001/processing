from flask import Flask, jsonify
from apscheduler.schedulers.background import BackgroundScheduler
import json
import requests
from datetime import datetime,timedelta
import logging
import yaml
import logging.config
import logging
import connexion
import pytz
from flask_cors import CORS, cross_origin

# Load configuration
with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

# Load configuration
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

# Initialize logger
logger = logging.getLogger('basicLogger')

def populate_stats():
    """ Periodically update statistics """
    logger.info("Start another new of Periodic Processing")
    
    # Read current statistics from the JSON file
    try:
        with open(app_config['datastore']['filename'], 'r') as file:
            stats_data = json.load(file)
    except FileNotFoundError:
        logger.error("Statistics do not exist")
        stats_data = {
            "num_deposit_reading": 0,
            "max_deposit_reading": 0,
            "number_account_id": 0,
            "max_withdrawal_reading": 0,
            "num_withdrawal_reading": 0,
            "last_updated": "2000-01-01T00:00:00Z"
        }

    try:
        response_deposit = requests.get(f"{app_config['deposit']['url']}{stats_data['last_updated']}")
        response_withdrawal = requests.get(f"{app_config['withdrawal']['url']}{stats_data['last_updated']}")
        
        if response_deposit.status_code == 200:
            deposit_list = response_deposit.json()
            logger.info(f"Received {len(deposit_list)} deposit events")
        else:
            logger.error("Failed to retrieve deposit events")
            deposit_list = []

        if response_withdrawal.status_code == 200:
            withdrawal_list = response_withdrawal.json()
            logger.info(f"Received {len(withdrawal_list)} withdrawal events")
        else:
            logger.error("Failed to retrieve withdrawal events")
            withdrawal_list = []

    except requests.exceptions.RequestException as error:
        logger.error(f"Failed to fetch events from Data Store Service: {str(error)}")
        deposit_list = []
        withdrawal_list = []

    # Debug prints
    print(f"deposit_list: {deposit_list}")
    print(f"withdrawal_list: {withdrawal_list}")

    # Check if new data is available
    if deposit_list or withdrawal_list:
        # Debug print
        print("Updating statistics")
        
        # Update statistics when new data is available
        if deposit_list:
            stats_data["num_deposit_reading"] += len(deposit_list)
            max_deposit_reading = max(deposit_list, key=lambda x: x['deposit_amount'])
            stats_data["max_deposit_reading"] = max(max_deposit_reading['deposit_amount'], stats_data["max_deposit_reading"])
            number_account_id = len(set(event['account_id'] for event in deposit_list))
            stats_data["number_account_id"] = number_account_id

        if withdrawal_list:
            stats_data["num_withdrawal_reading"] += len(withdrawal_list)
            max_withdrawal_reading = max(withdrawal_list, key=lambda x: x['withdrawal_amount'])
            stats_data["max_withdrawal_reading"] = max(max_withdrawal_reading['withdrawal_amount'], stats_data["max_withdrawal_reading"])

        stats_data["last_updated"] = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")

    # Write the updated statistics to the JSON file
    with open(app_config['datastore']['filename'], 'w') as file:
        json.dump(stats_data, file, indent=4)

    logger.debug("Updated statistics: %s", stats_data)
    logger.info("Periodic Processing Ended")

def get_stats():
    logger.info("Request for statistics has started")

    try:
        # Read current statistics from the JSON file
        with open(app_config['datastore']['filename'], 'r') as data_file:
            stats_data = json.load(data_file)
    except FileNotFoundError:
        logger.error("Statistics do not exist")
        return jsonify({"message": "Statistics do not exist"}), 404

    # Create a Python dictionary matching the response structure
    response_stats = {
        "num_deposit_reading": stats_data["num_deposit_reading"],
        "max_deposit_reading": stats_data["max_deposit_reading"],
        "number_account_id": stats_data["number_account_id"],
        "max_withdrawal_reading": stats_data["max_withdrawal_reading"],
        "num_withdrawal_reading": stats_data["num_withdrawal_reading"],
        "last_updated": stats_data["last_updated"]
    }

    logger.debug(f"Statistics response: {response_stats}")
    logger.info("Request for statistics has completed")

    return jsonify(response_stats), 200

def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats, 'interval', seconds=app_config['scheduler']['period_sec'])
    sched.start()

app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    # Run our standalone gevent server
    init_scheduler()
    app.run(port=8100, use_reloader=False)
